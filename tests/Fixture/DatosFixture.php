<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * DatosFixture
 */
class DatosFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'nombre' => 'Lorem ipsum dolor sit amet',
                'victorias' => 1,
                'derrotas' => 1,
            ],
        ];
        parent::init();
    }
}
