<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query\SelectQuery;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Datos Model
 *
 * @method \App\Model\Entity\Dato newEmptyEntity()
 * @method \App\Model\Entity\Dato newEntity(array $data, array $options = [])
 * @method array<\App\Model\Entity\Dato> newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Dato get(mixed $primaryKey, array|string $finder = 'all', \Psr\SimpleCache\CacheInterface|string|null $cache = null, \Closure|string|null $cacheKey = null, mixed ...$args)
 * @method \App\Model\Entity\Dato findOrCreate($search, ?callable $callback = null, array $options = [])
 * @method \App\Model\Entity\Dato patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method array<\App\Model\Entity\Dato> patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Dato|false save(\Cake\Datasource\EntityInterface $entity, array $options = [])
 * @method \App\Model\Entity\Dato saveOrFail(\Cake\Datasource\EntityInterface $entity, array $options = [])
 * @method iterable<\App\Model\Entity\Dato>|\Cake\Datasource\ResultSetInterface<\App\Model\Entity\Dato>|false saveMany(iterable $entities, array $options = [])
 * @method iterable<\App\Model\Entity\Dato>|\Cake\Datasource\ResultSetInterface<\App\Model\Entity\Dato> saveManyOrFail(iterable $entities, array $options = [])
 * @method iterable<\App\Model\Entity\Dato>|\Cake\Datasource\ResultSetInterface<\App\Model\Entity\Dato>|false deleteMany(iterable $entities, array $options = [])
 * @method iterable<\App\Model\Entity\Dato>|\Cake\Datasource\ResultSetInterface<\App\Model\Entity\Dato> deleteManyOrFail(iterable $entities, array $options = [])
 */
class DatosTable extends Table
{
    /**
     * Initialize method
     *
     * @param array<string, mixed> $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('datos');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->scalar('nombre')
            ->maxLength('nombre', 50)
            ->allowEmptyString('nombre');

        $validator
            ->integer('victorias')
            ->allowEmptyString('victorias');

        $validator
            ->integer('derrotas')
            ->allowEmptyString('derrotas');

        return $validator;
    }
}
