<!-- En tu archivo index.ctp -->
<h1>Datos de los Jugadores</h1>

<table>
    <tr>
        <th>Nombre del Jugador</th>
        <th>Cantidad de Partidas Ganadas</th>
        <th>Cantidad de Partidas Perdidas</th>
    </tr>
    <?php foreach ($datos as $dato): ?>
    <tr>
        <td><?= h($dato->nombre_jugador) ?></td>
        <td><?= h($dato->cantidad_ganadas) ?></td>
        <td><?= h($dato->cantidad_perdidas) ?></td>
    </tr>
    <?php endforeach; ?>
</table>
