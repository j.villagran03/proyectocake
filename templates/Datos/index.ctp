<!-- En tu vista index.ctp -->
<h1>Datos de los Jugadores</h1>

<table>
    <tr>
        <th>Nombre del Jugador</th>
        <th>Cantidad de Partidas Ganadas</th>
        <th>Cantidad de Partidas Perdidas</th>
    </tr>
    <?php foreach ($datos as $dato): ?>
    <tr>
        <td><?= h($dato->nombre_jugador) ?></td>
        <td><?= h($dato->cantidad_ganadas) ?></td>
        <td><?= h($dato->cantidad_perdidas) ?></td>
    </tr>
    <?php endforeach; ?>
</table>

<div class="paginator">
    <ul class="pagination">
        <?= $this->Paginator->first('<< ' . __('primero')) ?>
        <?= $this->Paginator->prev('< ' . __('anterior')) ?>
        <?= $this->Paginator->numbers() ?>
        <?= $this->Paginator->next(__('siguiente') . ' >') ?>
        <?= $this->Paginator->last(__('último') . ' >>') ?>
    </ul>
    <p><?= $this->Paginator->counter() ?></p>
</div>
