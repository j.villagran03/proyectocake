<!-- index.php -->
<!DOCTYPE html>
<html>
<head>
    <title>Datos de los Jugadores</title>
</head>
<body>

<h1>Datos de los Jugadores</h1>

<table>
    <tr>
        <th>Nombre del Jugador</th>
        <th>Cantidad de Partidas Ganadas</th>
        <th>Cantidad de Partidas Perdidas</th>
    </tr>
<?php foreach ($datosFormateados as $dato): ?>
    <tr>
        <td><?= h($dato['nombre']) ?></td>
        <td><?= h($dato['victorias']) ?></td>
        <td><?= h($dato['derrotas']) ?></td>
    </tr>
<?php endforeach; ?>
</table>

<?php var_dump($datos); ?>

</body>
</html>
